/* window.rs
 *
 * Copyright 2023 Apostol Bakalov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use gtk::prelude::*;
use adw::subclass::prelude::*;
use gtk::{gio, glib};
use glib::{clone, GString, Error};

mod imp {
    use super::*;

    #[derive(Debug, gtk::CompositeTemplate)]
    #[template(resource = "/com/examples/TextViewer/window.ui")]
    pub struct TextviewerWindow {
        // Template widgets
        #[template_child]
        pub header_bar: TemplateChild<gtk::HeaderBar>,
        #[template_child]
        pub text_view: TemplateChild<gtk::TextView>,
        #[template_child]
        pub toast_overlay: TemplateChild<adw::ToastOverlay>,
        #[template_child]
        pub cursor_pos: TemplateChild<gtk::Label>,

        pub settings: gio::Settings
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TextviewerWindow {
        const NAME: &'static str = "TextviewerWindow";
        type Type = super::TextviewerWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();

            klass.install_action("open-file", None, move |win, _, _| {
                win.open_file();
            });

            klass.install_action("save-as", None, move |win, _, _| {
                win.save_file();
            });
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }

        fn new() -> Self {
            Self {
                header_bar: TemplateChild::default(),
                text_view: TemplateChild::default(),
                toast_overlay: TemplateChild::default(),
                cursor_pos: TemplateChild::default(),
                settings: gio::Settings::new("com.examples.TextViewer")
            }
        }
    }

    impl ObjectImpl for TextviewerWindow {}
    impl WidgetImpl for TextviewerWindow {}
    impl WindowImpl for TextviewerWindow {}
    impl ApplicationWindowImpl for TextviewerWindow {}
    impl AdwApplicationWindowImpl for TextviewerWindow {}
}

glib::wrapper! {
    pub struct TextviewerWindow(ObjectSubclass<imp::TextviewerWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl TextviewerWindow {
    pub fn new<P: glib::IsA<gtk::Application>>(application: &P) -> Self {
        let window = glib::Object::new::<Self>(&[("application", application)]);

        window.connect_signals();
        window.init_settings();

        window
    }

    fn connect_signals(&self) {
        let buffer = self.imp().text_view.buffer();
        buffer.connect_cursor_position_notify(clone!(@weak self as win => move |_| {
            win.update_cursor_pos();
        }));
    }

    fn init_settings(&self) {
         self.imp().settings
            .bind("window-width", self, "default-width")
            .flags(gio::SettingsBindFlags::DEFAULT)
            .build();
        self.imp().settings
            .bind("window-height", self, "default-height")
            .flags(gio::SettingsBindFlags::DEFAULT)
            .build();
        self.imp().settings
            .bind("window-maximized", self, "maximized")
            .flags(gio::SettingsBindFlags::DEFAULT)
            .build();
    }

    fn update_cursor_pos(&self) {
        let buff = self.imp().text_view.buffer();

        let iter = buff.iter_at_offset(buff.cursor_position());

        let line = iter.line();
        let line_offset = iter.line_offset();
        let label = format!("Ln {}, Col {}", line, line_offset);

        self.imp().cursor_pos.set_label(&label);
    }

    fn open_file(&self) {
        let dialog = gtk::FileChooserNative::builder()
            .accept_label("_Open")
            .cancel_label("_Cancel")
            .modal(true)
            .title("Open File")
            .action(gtk::FileChooserAction::Open)
            .select_multiple(false)
            .transient_for(self)
            .build();

        let filter = gtk::FileFilter::new();
        gtk::FileFilter::set_name(&filter, Some("Text files"));
        filter.add_mime_type("text/*");
        dialog.add_filter(&filter);

        dialog.connect_response(
            clone!(@strong dialog, @weak self as win => move |_, response| {
                if response == gtk::ResponseType::Accept {
                    let selected_file = dialog.file().unwrap();
                    let file_name = win.get_filename(&selected_file);

                    selected_file.load_contents_async(
                        gio::Cancellable::NONE,
                        move |res: Result<(Vec<u8>, Option<GString>), Error>| {
                            win.open_file_complete(res, file_name);
                    });
                }
            }),
        );

        dialog.show();
    }

    fn open_file_complete(&self, result: Result<(Vec<u8>, Option<GString>), Error>, file_name: String) {
        match result {
            Err(_) => {
                let toast_msg = format!("Cannot open {}", file_name);
                self.show_toast_msg(&toast_msg);
            },
            Ok(res) => {
                let txt = String::from_utf8(res.0).unwrap();
                self.imp().text_view.buffer().set_text(txt.as_str());
                self.set_title(Some(&file_name));
                let toast_msg = format!("Opened {}", file_name);
                self.show_toast_msg(&toast_msg);
            }
        }
    }

    fn save_file(&self) {

        let dialog = gtk::FileChooserNative::builder()
            .accept_label("_Save")
            .cancel_label("_Cancel")
            .modal(true)
            .title("Save File As")
            .action(gtk::FileChooserAction::Save)
            .select_multiple(false)
            .transient_for(self)
            .build();

        dialog.connect_response(
            clone!(@strong dialog, @weak self as win => move |_, response| {
                if response == gtk::ResponseType::Accept {
                    let selected_file = dialog.file().unwrap();
                    let file_name = win.get_filename(&selected_file);

                    let buffer = win.imp().text_view.buffer();
                    let contents = buffer.text(&buffer.start_iter(), &buffer.end_iter(), false);
                    let bytes = glib::GString::from(contents);

                    selected_file.replace_contents_async(bytes,
                        Option::None,
                        false,
                        gio::FileCreateFlags::NONE,
                        gio::Cancellable::NONE,
                        move |res: Result<(GString, GString), (GString, Error)>| {
                            win.save_file_complete(res, file_name);
                    });
                }
            }),
        );

        dialog.show();
    }

    fn save_file_complete(&self, res: Result<(GString, GString), (GString, Error)>, file_name: String) {
        let is_success = match res {
            Err(_) => false,
            Ok(_) => true
        };

        let mut toast_msg = format!("Saved file {}", file_name);

        if !is_success {
            toast_msg = "Unable to save file".to_string();
        }

        self.show_toast_msg(&toast_msg);
    }

    fn show_toast_msg(&self, toast_msg: &str) {
        let toast = adw::Toast::new(&toast_msg);
        self.imp().toast_overlay.add_toast(&toast);
    }

    fn get_filename(&self, file: &gio::File) -> String {
        let file_info = file.query_info("standard::display-name", gio::FileQueryInfoFlags::NONE, gio::Cancellable::NONE);
        let file_display_name = match file_info {
            Ok(v) => String::from(v.attribute_string("standard::display-name").unwrap().as_str()),
            Err(_) => file.basename().unwrap().into_os_string().into_string().unwrap()
        };

        return file_display_name;
    }
}
