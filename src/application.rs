/* application.rs
 *
 * Copyright 2023 Apostol Bakalov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use gtk::prelude::*;
use adw::subclass::prelude::*;
use gtk::{gio, glib};
use glib::{clone};

use crate::config::VERSION;
use crate::TextviewerWindow;

mod imp {
    use super::*;

    #[derive(Debug)]
    pub struct TextviewerApplication {
        pub settings: gio::Settings
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TextviewerApplication {
        const NAME: &'static str = "TextviewerApplication";
        type Type = super::TextviewerApplication;
        type ParentType = adw::Application;

        fn new() -> Self {
            let app_settings = gio::Settings::new("com.examples.TextViewer");

            Self {
                settings: app_settings
            }
        }
    }

    impl ObjectImpl for TextviewerApplication {

        fn constructed(&self) {
            self.parent_constructed();

            let obj = self.instance();
            obj.setup_gactions();
            obj.set_accels_for_action("app.quit", &["<primary>q"]);
        }
    }

    impl ApplicationImpl for TextviewerApplication {
        // We connect to the activate callback to create a window when the application
        // has been launched. Additionally, this callback notifies us when the user
        // tries to launch a "second instance" of the application. When they try
        // to do that, we'll just present any existing window.
        fn activate(&self) {
            let application = self.instance();

            // Get the current window or create one if necessary
            let window = if let Some(window) = application.active_window() {
                window
            } else {
                let window = TextviewerWindow::new(&*application);
                window.upcast()
            };

            // Ask the window manager/compositor to present the window
            window.present();

            // Switch to the last used theme
            let is_in_dark_mode = self.settings.boolean("dark-mode");
            application.change_color_scheme(is_in_dark_mode);
        }
    }

    impl GtkApplicationImpl for TextviewerApplication {}
    impl AdwApplicationImpl for TextviewerApplication {}
}

glib::wrapper! {
    pub struct TextviewerApplication(ObjectSubclass<imp::TextviewerApplication>)
        @extends gio::Application, gtk::Application, adw::Application,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl TextviewerApplication {
    pub fn new(application_id: &str, flags: &gio::ApplicationFlags) -> Self {
        glib::Object::new(&[("application-id", &application_id), ("flags", flags)])
    }

    fn setup_gactions(&self) {
        let quit_action = gio::ActionEntry::builder("quit")
            .activate(move |app: &Self, _, _| app.quit())
            .build();
        let about_action = gio::ActionEntry::builder("about")
            .activate(move |app: &Self, _, _| app.show_about())
            .build();

        self.add_action_entries([quit_action, about_action]).unwrap();

        let is_in_dark_mode = self.imp().settings.boolean("dark-mode");
        let dark_mode_action = gio::SimpleAction::new_stateful("dark-mode",
                                                              None,
                                                              &is_in_dark_mode.to_variant());

        dark_mode_action.connect_activate(
            clone!(@weak self as app => move |action, _| {
                app.toggle_dark_mode(action);
            })
        );

        dark_mode_action.connect_change_state(
            clone!(@weak self as app => move |action, new_state| {
                app.change_color_scheme_from_action(action, new_state.unwrap());
            })
        );

        self.add_action(&dark_mode_action);
    }

    fn show_about(&self) {
        let window = self.active_window().unwrap();
        let about = adw::AboutWindow::builder()
            .transient_for(&window)
            .application_name("Text Viewer")
            .application_icon("com.examples.TextViewer")
            .developer_name("Example Author")
            .version(VERSION)
            .developers(vec!["Example Author".into()])
            .copyright("© 2023 Example Author")
            .build();

        about.present();
    }

    fn toggle_dark_mode(&self, action: &gio::SimpleAction) {
        let state = action.state().unwrap();

        let old_state = match state.get::<bool>() {
            Some(b) => b,
            None => false
        };

        let new_state = !old_state;
        action.change_state(&new_state.to_variant());
    }

    fn change_color_scheme_from_action(&self, action: &gio::SimpleAction, new_state: &glib::variant::Variant) {
        let is_dark_mode = match new_state.get::<bool>() {
            Some(state) => state,
            None => false
        };

        self.change_color_scheme(is_dark_mode);

        action.set_state(new_state);

        _ = self.imp().settings.set_boolean("dark-mode", is_dark_mode);
    }

    fn change_color_scheme(&self, is_dark_mode: bool) {
        let style_manager = adw::StyleManager::default();

        if is_dark_mode {
            style_manager.set_color_scheme(adw::ColorScheme::ForceDark);
        } else {
            style_manager.set_color_scheme(adw::ColorScheme::ForceLight);
        }
    }
}
